<?php

namespace App;

use App\Http\Resources\BuyerResource;
use App\User;
use App\Transaction;
use App\Scopes\BuyerScope;

class Buyer extends User
{
    public $resource = BuyerResource::class;
    
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new BuyerScope);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','email'
    ];
}
