<?php

namespace App;

use App\Buyer;
use App\Http\Resources\TransactionResource;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'quantity',
        'buyer_id',
        'product_id',
    ];

    public $resource = TransactionResource::class;

    public function buyer(){
        return $this->belongsTo(Buyer::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
