<?php

namespace App\Http\Controllers\Transaction;

use App\Seller;
use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionSellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function index(Transaction $transaction)
    {
      $seller = $transaction->product->seller()->paginate($this->determinatePageSize());

      return $this->showOne($seller);
    }
}
