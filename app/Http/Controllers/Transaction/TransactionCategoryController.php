<?php

namespace App\Http\Controllers\Transaction;

use App\Category;
use App\Transaction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TransactionCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function index(Transaction $transaction)
    {
      $categories = $transaction->product->categories()->paginate($this->determinatePageSize());

      return $this->showAll($categories);
    }
}
