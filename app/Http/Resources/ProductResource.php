<?php

namespace App\Http\Resources;

use App\Http\Resources\BaseResource;

class ProductResource extends BaseResource
{
    
    public static $map = [
        'id' => 'identifier',
        'name' => 'title',
        'description' => 'details',
        'quantity' => 'stock',
        'status' => 'situacion',
        'seller_id' => 'seller',
        'updated_at' => 'last_modified',
        'created_at' => 'creation_date',
    ]; 

    public function generateLinks($request){
        return [
            [
                'rel' => 'self',
                'href' => route('products.show', $this->id),
            ],
            [
                'rel' => 'product.buyers',
                'hreft' => route('products.buyers.index', $this->id),
            ],
            [
                'rel' => 'product.categories',
                'hreft' => route('products.categories.index', $this->id),
            ],
            [
                'rel' => 'product.transactions',
                'hreft' => route('products.transactions.index', $this->id),
            ],
            [
                'rel' => 'seller.show',
                'hreft' => route('sellers.show', $this->seller_id),
            ],

        ];
    }
}
