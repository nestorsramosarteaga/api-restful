<?php

namespace App\Http\Resources;

use App\Http\Resources\BaseResource;

class SellerResource extends BaseResource
{
    
    public static $map = [
        'id' => 'identifier',
        'name' => 'full_name',
        'email' => 'email_address',
        'updated_at' => 'last_modified',
        'created_at' => 'creation_date',
    ];

    public function generateLinks($request){
        return [
            [
                'rel' => 'self',
                'href' => route('sellers.show', $this->id),
            ],
            [
                'rel' => 'seller.buyers',
                'hreft' => route('sellers.buyers.index', $this->id),
            ],
            [
                'rel' => 'seller.categories',
                'hreft' => route('sellers.categories.index', $this->id),
            ],
            [
                'rel' => 'seller.products',
                'hreft' => route('sellers.products.index', $this->id),
            ],
            [
                'rel' => 'seller.transactions',
                'hreft' => route('sellers.transactions.index', $this->id),
            ],
            [
                'rel' => 'user',
                'hreft' => route('users.show', $this->id),
            ],
        ];
    }

}
