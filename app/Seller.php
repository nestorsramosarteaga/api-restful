<?php

namespace App;

use App\Http\Resources\SellerResource;
use App\User;
use App\Product;
use App\Scopes\SellerScope;

class Seller extends User   
{
    public $resource = SellerResource::class;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SellerScope);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','email'
    ];
}
